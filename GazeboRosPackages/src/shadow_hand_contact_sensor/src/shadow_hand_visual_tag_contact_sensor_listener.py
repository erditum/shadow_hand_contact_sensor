#!/usr/bin/env python
import rospy
import rospkg
from shadow_hand_contact_sensor.msg import shadow_hand_contact_force
import numpy as np
import os 
def callback(data):
    global contact_names
    if not contact_names:
        contact_names = {key: [] for key in data.contact_visual_tag_name}
    for c_n in data.contact_visual_tag_name:
        index_c_n = list(contact_names).index(c_n)
        contact_names[c_n].append(data.force_array[index_c_n].x+data.force_array[index_c_n].y+data.force_array[index_c_n].z)
    
def listener():
    rospy.init_node('shadow_hand_visual_tag_contact_sensor_listener', anonymous=True)

    counter = 0 
    rospy.Subscriber("shadow_hand_visual_tag_contact_sensor", shadow_hand_contact_force, callback)
    rate = rospy.Rate(100) # 100hz
    while not rospy.is_shutdown():
        rate.sleep()
        np.save(path_of_shadow_hand_package+"/data/contact_names.npy",contact_names)

    rospy.spin()

if __name__ == '__main__':
    rospack = rospkg.RosPack()
    path_of_shadow_hand_package = rospack.get_path('shadow_hand_contact_sensor')
    
    if not os.path.exists(path_of_shadow_hand_package+"/data"):
        os.mkdir(path_of_shadow_hand_package+"/data")

    contact_names = {}
    listener()
