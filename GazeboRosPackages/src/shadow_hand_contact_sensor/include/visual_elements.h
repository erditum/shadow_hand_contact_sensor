//
// Created by erdi on 31.05.21.
//

#ifndef SHADOW_HAND_CONTACT_SENSOR_VISUAL_ELEMENTS_H
#define SHADOW_HAND_CONTACT_SENSOR_VISUAL_ELEMENTS_H

#include<iostream>
#include<string>

std::string edge1_cube_sdf_string ="<sdf version ='1.4'>\
<model name='boundary_box_visual_cube'>\
<static>1</static>\
<pose>0 0 0 0 0 0 </pose>\
<link name='link'>\
<pose frame=''>0 0 0 0 -0 0</pose>\
<visual name='visual'>\
<geometry>\
<box>\
<size>0.001 0.001 0.001</size>\
</box>\
</geometry>\
<material>\
<ambient>0.1 0.1 0.1 1</ambient>\
<diffuse>0 1 1 1</diffuse>\
<specular>0 0 0 0</specular>\
<emissive>0 0 0 1</emissive>\
</material>\
</visual>\
</link>\
</model>\
</sdf>";

std::string edge2_cube_sdf_string ="<sdf version ='1.4'>\
<model name='boundary_box_visual_cube'>\
<static>1</static>\
<pose>0 0 0 0 0 0 </pose>\
<link name='link'>\
<pose frame=''>0 0 0 0 -0 0</pose>\
<visual name='visual'>\
<geometry>\
<box>\
<size>0.001 0.001 0.001</size>\
</box>\
</geometry>\
<material>\
<ambient>0.1 0.1 0.1 1</ambient>\
<diffuse>0 1 1 1</diffuse>\
<specular>0 0 0 0</specular>\
<emissive>0 0 0 1</emissive>\
</material>\
</visual>\
</link>\
</model>\
</sdf>";

std::string edge3_cube_sdf_string ="<sdf version ='1.4'>\
<model name='boundary_box_visual_cube'>\
<static>1</static>\
<pose>0 0 0 0 0 0 </pose>\
<link name='link'>\
<pose frame=''>0 0 0 0 -0 0</pose>\
<visual name='visual'>\
<geometry>\
<box>\
<size>0.001 0.001 0.001</size>\
</box>\
</geometry>\
<material>\
<ambient>0.1 0.1 0.1 1</ambient>\
<diffuse>0 1 1 1</diffuse>\
<specular>0 0 0 0</specular>\
<emissive>0 0 0 1</emissive>\
</material>\
</visual>\
</link>\
</model>\
</sdf>";

std::string edge4_cube_sdf_string ="<sdf version ='1.4'>\
<model name='boundary_box_visual_cube'>\
<static>1</static>\
<pose>0 0 0 0 0 0 </pose>\
<link name='link'>\
<pose frame=''>0 0 0 0 -0 0</pose>\
<visual name='visual'>\
<geometry>\
<box>\
<size>0.001 0.001 0.001</size>\
</box>\
</geometry>\
<material>\
<ambient>0.1 0.1 0.1 1</ambient>\
<diffuse>0 1 1 1</diffuse>\
<specular>0 0 0 0</specular>\
<emissive>0 0 0 1</emissive>\
</material>\
</visual>\
</link>\
</model>\
</sdf>";

std::string edge5_cube_sdf_string ="<sdf version ='1.4'>\
<model name='boundary_box_visual_cube'>\
<static>1</static>\
<pose>0 0 0 0 0 0 </pose>\
<link name='link'>\
<pose frame=''>0 0 0 0 -0 0</pose>\
<visual name='visual'>\
<geometry>\
<box>\
<size>0.001 0.001 0.001</size>\
</box>\
</geometry>\
<material>\
<ambient>0.1 0.1 0.1 1</ambient>\
<diffuse>0 1 1 1</diffuse>\
<specular>0 0 0 0</specular>\
<emissive>0 0 0 1</emissive>\
</material>\
</visual>\
</link>\
</model>\
</sdf>";

std::string edge6_cube_sdf_string ="<sdf version ='1.4'>\
<model name='boundary_box_visual_cube'>\
<static>1</static>\
<pose>0 0 0 0 0 0 </pose>\
<link name='link'>\
<pose frame=''>0 0 0 0 -0 0</pose>\
<visual name='visual'>\
<geometry>\
<box>\
<size>0.001 0.001 0.001</size>\
</box>\
</geometry>\
<material>\
<ambient>0.1 0.1 0.1 1</ambient>\
<diffuse>0 1 1 1</diffuse>\
<specular>0 0 0 0</specular>\
<emissive>0 0 0 1</emissive>\
</material>\
</visual>\
</link>\
</model>\
</sdf>";

std::string edge7_cube_sdf_string ="<sdf version ='1.4'>\
<model name='boundary_box_visual_cube'>\
<static>1</static>\
<pose>0 0 0 0 0 0 </pose>\
<link name='link'>\
<pose frame=''>0 0 0 0 -0 0</pose>\
<visual name='visual'>\
<geometry>\
<box>\
<size>0.001 0.001 0.001</size>\
</box>\
</geometry>\
<material>\
<ambient>0.1 0.1 0.1 1</ambient>\
<diffuse>0 1 1 1</diffuse>\
<specular>0 0 0 0</specular>\
<emissive>0 0 0 1</emissive>\
</material>\
</visual>\
</link>\
</model>\
</sdf>";

std::string edge8_cube_sdf_string ="<sdf version ='1.4'>\
<model name='boundary_box_visual_cube'>\
<static>1</static>\
<pose>0 0 0 0 0 0 </pose>\
<link name='link'>\
<pose frame=''>0 0 0 0 -0 0</pose>\
<visual name='visual'>\
<geometry>\
<box>\
<size>0.001 0.001 0.001</size>\
</box>\
</geometry>\
<material>\
<ambient>0.1 0.1 0.1 1</ambient>\
<diffuse>0 1 1 1</diffuse>\
<specular>0 0 0 0</specular>\
<emissive>0 0 0 1</emissive>\
</material>\
</visual>\
</link>\
</model>\
</sdf>";



#endif //SHADOW_HAND_CONTACT_SENSOR_VISUAL_ELEMENTS_H
