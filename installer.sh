#!/bin/bash

TARGET_FOLDER_NRP_ROS=$HBP

cp -Ru GazeboRosPackages ${TARGET_FOLDER_NRP_ROS}
cp -Ru Models ${TARGET_FOLDER_NRP_ROS}
cp -Ru Experiments ${TARGET_FOLDER_NRP_ROS}

sudo apt-get update

# sudo apt install -y  ros-${ROS_DISTRO}-moveit
# sudo apt install -y ros-${ROS_DISTRO}-moveit-visual-tools
cd ${TARGET_FOLDER_NRP_ROS}/GazeboRosPackages
if [[ ${ROS_DISTRO} == "noetic" ]]; then
    echo "Ros packages for ${ROS_DISTRO} are built"
    catkin build
   
elif [[ ${ROS_DISTRO} == "melodic" || ${ROS_DISTRO} == "kinetic" ]]; then
    echo "Ros packages for ${ROS_DISTRO} are built"
    catkin_make
else
    echo "Unknown ROS DISTRIBUTION"
fi
rosdep install -y shadow_hand_contact_sensor
cd ${NRP_MODELS_DIRECTORY}
./create-symlinks.sh
